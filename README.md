# k8s-hetzner-cloud

Kubernetes Terraform installer for Hetzner bare-metal.

![k8s-cluster](docs/images/k8s-cluster.PNG)

### Initial setup

Clone the repository and install the dependencies:

```bash
$ git clone https://gitlab.com/a_achter_cicd/hcloud-k8s-tf.git
$ cd k8s-hetzner
$ terraform init
```

Note that you'll need Terraform v0.13 or newer to run this project.

Also note that, before running the project, you'll have to create an access token for Terraform to connect to the Hetzner Cloud API.

Using the token, create the `TF_VAR_hcloud_token` as a Gitlab Variable.

Also create Gitlab variables for the private and public ssh keys

See wiki page for more info: https://gitlab.com/a_achter_cicd/hcloud-k8s-tf/-/wikis/ci-variables


### Building the Kubernetes VM image with Packer

```cli
$ cd images && \
    packer build image-master.json
```

### Usage

Create a bare-metal Kubernetes cluster with one master and 2 worker nodes:

```bash

The jobs in Gitlab CI will do the following:

- provisions two bare-metal servers with Ubuntu 20.04 LTS (the size of the `master` and the `node` may be different but must remain in the same type of architecture)
- connects to the master server via SSH and installs Docker CE and kubeadm apt packages
- runs kubeadm init on the master server and configures kubectl
- downloads the kubectl admin config file on your local machine and replaces the private IP with the public one
- creates a Kubernetes secret with the Weave Net password
- installs Weave Net with encrypted overlay
- starts the nodes in parallel and installs Docker CE and kubeadm
- joins the nodes in the cluster using the kubeadm token obtained from the master


```bash

```

Scale up by increasing the number of nodes:
 var.worker_nodes_count=4

